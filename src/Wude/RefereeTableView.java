package Wude;

import Wude.Objects.Referee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RefereeTableView {

    RefereeTableView(Referee[] aar, TableView<Referee> refereeTableViewTT, HBox hBox) {

        Integer iID;
        TextField nm, family, birthday;
//Define columns
        TableColumn<Referee, Integer> idColumn = new TableColumn("ID");
        idColumn.setMinWidth(25);
        idColumn.setCellValueFactory(new PropertyValueFactory<Referee, Integer>("personID"));

        TableColumn<Referee, String> nameColumn = new TableColumn("Name");
        nameColumn.setMinWidth(100);
        nameColumn.setCellValueFactory(new PropertyValueFactory<Referee, String>("name"));

        TableColumn<Referee, String> familyColumn = new TableColumn("Family");
        familyColumn.setMinWidth(100);
        familyColumn.setCellValueFactory(new PropertyValueFactory<Referee, String>("family"));

        TableColumn<Referee, java.sql.Date> birthColumn = new TableColumn("Birthday");
        birthColumn.setMinWidth(150);
        birthColumn.setCellValueFactory(new PropertyValueFactory<Referee, java.sql.Date>("birthday"));

// Field inputs
        nm = new TextField();
        nm.setPromptText("Name");
        nm.setMinWidth(100);

        family = new TextField();
        family.setPromptText("Family");
        family.setMinWidth(100);

        birthday = new TextField();
        birthday.setPromptText("yyyy-mm-dd");
        birthday.setMinWidth(100);

//        Buttons
        final Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            addButtonClicked(aar, nm.getText(), family.getText(), birthday.getText(), refereeTableViewTT);
        });

        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> deleteButtonClicked(aar, refereeTableViewTT));

        hBox.setPadding(new javafx.geometry.Insets(10, 10, 10, 10));
        hBox.setSpacing(10);
        hBox.getChildren().addAll(nm, family, birthday, addButton, deleteButton);

//       refereeTableViewTT= new TableView<Referee>();

//        refereeTableViewTT.setItems(getReferees(aar));
        refereeTableViewTT.setItems(Wude.newCompetitionSetup.refereesObservableList);

        refereeTableViewTT.getColumns().addAll(idColumn, nameColumn, familyColumn, birthColumn);
        refereeTableViewTT.setEditable(true);
        refereeTableViewTT.setTableMenuButtonVisible(true);
    }

    private void deleteButtonClicked(Referee[] mas, TableView<Referee> refereeTableViewTT) {

        mas[refereeTableViewTT.getSelectionModel().getSelectedIndex()] = null;
//        refereeTableViewTT.setItems(getReferees(mas));
        refereeTableViewTT.setItems(Wude.newCompetitionSetup.refereesObservableList);

    }

    private void addButtonClicked(Referee[] mas, String name, String fam, String dt, TableView<Referee> refereeTableViewTT) {
        java.sql.Date ddd = null;
        int i = 1;
        while (mas[i] != null) {
            i++;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = null;
        try {
            parsed = format.parse(dt);
            ddd = new java.sql.Date(parsed.getTime());
        } catch (ParseException e) {
        } finally {
//            mas[i] = new Referee(i, name, fam, ddd, 0, 0);
        }
        refereeTableViewTT.setItems(getReferees(mas));
    }

    public ObservableList<Referee> getReferees(Referee[] aaa) {
        ObservableList<Referee> refs = FXCollections.observableArrayList();
        int i = 1;
        while (i <= aaa.length) {
            try {
                if (aaa[i].getName() != "") {
                    refs.add(aaa[i]);
                }
            } catch (NullPointerException e) {
                i = aaa.length;
            } finally {
                i++;
            }
        }
        return refs;
    }


}
