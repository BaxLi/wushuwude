package Wude;


import Wude.Objects.CurrentCompetition;
import Wude.Objects.Participant;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import com.itextpdf.text.*;

public class Wude extends Application {
/* declaration phase - read all DB and create an arrays of objects like referee, participant, etc..
 */

    Document document = new Document(PageSize.A4, 50, 50, 50, 50);


    public static Scene[] scenesArrayList= new Scene[51];
    public static Stage mainWindowsStage;

    public static Image logoImagePic;

    public final static int REFEREE_MaxQty =20;
    public static CurrentCompetition newCompetitionSetup =new CurrentCompetition();


    @FXML public static BorderPane mainBorderPane;
    public ToolBar tb=new ToolBar();

    //Void constructor - for initializing purposes
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{


       logoImagePic= new Image(Wude.class.getResourceAsStream("Pictures/ico1-1.jpg"));

        mainWindowsStage=primaryStage;
        scenesArrayList[1]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/Wude.fxml")), 1024,768); //MainScene
        scenesArrayList[2]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/RefereeTableView.fxml")), 1024,768);
        scenesArrayList[3]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/CompetitionWizard.fxml")), 1024,768);
        scenesArrayList[4]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/CompetitionWEBCamera.fxml")), 1024,768);
        scenesArrayList[10]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/RefereeAddNewView.fxml")), 1024,768); // Add new referee object form
        scenesArrayList[50]=new Scene(FXMLLoader.load(getClass().getResource("FXMLFiles/TestCheckCompetition dates.fxml")), 1024,768);
        // Kostili #1  - naimenovanie komandi initializiruem

        for (Participant o:newCompetitionSetup.participantsObservableList)
        {
            o.setTeam(String.valueOf(newCompetitionSetup.teamsObservableList.get(o.getTeamID()-1).getTeamName()));
        }
        primaryStage.setTitle("WUDE Federation of RM");
        primaryStage.setResizable(true);
// --------------------------------Kostili  �1

        /*
        set icons
        */
        Image ico = new Image(Wude.class.getResourceAsStream("Pictures/ico1.jpg"));
        primaryStage.getIcons().add(ico);
        /*
        set background picture
        */
        Image bckImg = new Image(Wude.class.getResourceAsStream("Pictures/background.jpg"));
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(bckImg, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER, backgroundSize);

//        Scene scene=new Scene(panel, 800,600);.

        primaryStage.setScene(scenesArrayList[1]);
        primaryStage.show();
    }

}
