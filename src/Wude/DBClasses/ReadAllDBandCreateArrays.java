


import javax.swing.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
 * Created by BaxLi on 07-Jul-15.
 */
public class ReadAllDBandCreateArrays {

   public static void readAllDBandCreateArrays() {
        String dbAdress = "jdbc:mysql://localhost:3306/test";
        String dbUserName = "BaxLi";
        String dbPass = "Qwert12345!";
        String query;
        Connection dbConnection = null;
        ResultSet dbResultSet = null;

        int i = 1;
        int j;
        int columnID, columnName, columnFamily, columnBirthday, columnLevel, columnAssociation;
        columnID = columnName = columnFamily = columnBirthday = columnLevel = columnAssociation = 0;
        java.sql.Date birth = null;

//        ------------ End var definitions

//        check if connection can be established
        try {
            dbConnection = DriverManager.getConnection(dbAdress, dbUserName, dbPass);
            if (dbConnection == null) {

//                dbStatement = dbConnection.createStatement();

// create Referee array in memory
                query = "SELECT * FROM referees";

                //Execute the query
//                dbResultSet = dbStatement.executeQuery(query);

//Loop through the results
                {
                    {
                        ResultSetMetaData columns = dbResultSet.getMetaData();
                        i = 1;
                        while (i <= columns.getColumnCount()) {
                            if (columns.getColumnName(i).equals("ID")) {
                                columnID = i;
                            } else if (columns.getColumnName(i).equals("Name")) {
                                columnName = i;
                            } else if (columns.getColumnName(i).equals("Family")) {
                                columnFamily = i;
                            } else if (columns.getColumnName(i).equals("Birthday")) {
                                columnBirthday = i;
                            } else if (columns.getColumnName(i).equals("Level")) {
                                columnLevel = i;
                            } else if (columns.getColumnName(i).equals("Association")) {
                                columnAssociation = i;
                            }
                            i++;
                        }
                    }
                    i = 1;
                    while (dbResultSet.next()) {
                        try {
                            Integer id = Integer.parseInt((dbResultSet.getString(columnID)));
                            String name = dbResultSet.getString(columnName);
                            String family = dbResultSet.getString(columnFamily);

//                          birthday convert string to Date

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            java.util.Date parsed = null;
                            try {
                                if (dbResultSet.getString(columnBirthday) != null) {
                                    parsed = format.parse(dbResultSet.getString(columnBirthday));
                                    birth = new java.sql.Date(parsed.getTime());
                                }
                            } catch (ParseException e) {
                                JOptionPane.showMessageDialog(null, "Birthday convert missed.Error code DI 122");
//                                e.printStackTrace();
                            } finally {
                                birth = null;
                            }
                            int level = Integer.parseInt(dbResultSet.getString(columnLevel));
                            int ass = Integer.parseInt(dbResultSet.getString(columnAssociation));
//                            create referee object
//                            refereeArray[i] = new Referee(id, name, family, birth, level, ass);


                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Can't create referee instance. Error code DI 125");
                            System.exit(0);
                        }

//                        JOptionPane.showMessageDialog(null, dbResultSet.getString(1) + " " + dbResultSet.getString(2) + " " + dbResultSet.getString(3) + " " + dbResultSet.getString(4));
                        i++;
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Can't establish connection with DB at the adress " + dbAdress + " using Username - " + dbUserName + " and Password " + dbPass);
            e.printStackTrace();
        } finally {
            try {
                if (dbResultSet != null) {
                    dbResultSet.close();
                }
//                if (dbStatement != null) {
//                    dbStatement.close();
//                }
//                if (dbConnection != null) {
//                    dbConnection.close();
//                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Can't close DB connection after reading! Errorcode DB-5. ");
                System.exit(0);
            }
        }
    }
}
