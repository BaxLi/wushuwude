package Wude.DBClasses;

import Wude.Objects.Associations;
import Wude.Objects.Participant;
import javafx.collections.ObservableList;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by BaxLi on 10/31/2015.
 */
public class ReadWriteExcelFile {

    public static void readParticipantsFromExcel(String fName, ObservableList<Participant> participants) throws IOException {

        if (checkFileToOpen(fName) == false) return;

        XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(fName));
        XSSFSheet myExcelSheet = myExcelBook.getSheet("Sheet1");
        XSSFRow row = myExcelSheet.getRow(1);

        int i = 0;
        int j = 1;
        while (row != null) {

            if ((row.getCell(1).getStringCellValue().equals("")) || (row.getCell(1).getStringCellValue() == null)) {
                //JOptionPane.showMessageDialog(null, "Name missed - will skip import of this participant");
                j++;
                row = myExcelSheet.getRow(j);
                continue;
            } else {
                //Integer id, String name, String family, String patronymic, String birth, Integer team

                String name = row.getCell(1).getStringCellValue();
                String family = row.getCell(2).getStringCellValue();
                String patronymic = row.getCell(3).getStringCellValue();
                String idnp;

                try {
                    idnp = row.getCell(6).getStringCellValue();
                } catch (Exception e) {
                    idnp = "";
                }
                String birthday = "";
                try {
                    Date birthdate = row.getCell(4).getDateCellValue();
                    SimpleDateFormat sdtf = new SimpleDateFormat("d/MM/y", Locale.getDefault());
                    birthday = String.valueOf(sdtf.format(birthdate));
                } catch (Exception e) {
                }
                Participant tmpParticipant = new Participant(participants.size() + 1, name, family, patronymic, birthday, 1, idnp);

                participants.add(tmpParticipant);
            }
            j++;
            row = myExcelSheet.getRow(j);
        }

//
//        if (row.getCell(0).getCellType() == HSSFCell.CELL_TYPE_STRING) {
//            String name = row.getCell(0).getStringCellValue();
//            System.out.println("name : " + name);
//        }
//
//        if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//            Date birthdate = row.getCell(1).getDateCellValue();
//            System.out.println("birthdate :" + birthdate);
//        }

        myExcelBook.close();

    }


    private static boolean checkFileToOpen(String fName) throws IOException {

        XSSFWorkbook myExcelBook = null;
        XSSFSheet myExcelSheet;
        XSSFRow row;

        try {
            FileInputStream tmpA = new FileInputStream(fName);
            myExcelBook = new XSSFWorkbook(tmpA);
            myExcelSheet = myExcelBook.getSheet("Sheet1");
            row = myExcelSheet.getRow(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Can't read from asssociations.xlxs,");
            return false;
        }

        try {
            myExcelBook.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Can't close cheked file " + fName);
            ;
        }

        return true;
    }

    public static void readAssociationsFromExcel(String file, ObservableList<Associations> associat) throws IOException {

        XSSFWorkbook myExcelBook = null;
        XSSFSheet myExcelSheet = null;
        XSSFRow row = null;

        if (checkFileToOpen(file) == false) return;

        myExcelBook = new XSSFWorkbook(new FileInputStream(file));
        myExcelSheet = myExcelBook.getSheet("Sheet1");
        row = myExcelSheet.getRow(1);


        int i = 0;
        int j = 1;
        while (row != null) {
            if ((row.getCell(1).getStringCellValue().equals("")) || (row.getCell(1).getStringCellValue() == null)) {
                //JOptionPane.showMessageDialog(null, "Name missed - will skip import of this participant");
                j++;
                row = myExcelSheet.getRow(j);
                continue;
            } else {
                //Integer id, String name, String family, String patronymic, String birth, Integer team

                String name = row.getCell(1).getStringCellValue();
                Associations tmpAssociat = new Associations(associat.size() + 1, name);
                associat.add(tmpAssociat);
            }
            j++;
            row = myExcelSheet.getRow(j);
        }

//
//        if (row.getCell(0).getCellType() == HSSFCell.CELL_TYPE_STRING) {
//            String name = row.getCell(0).getStringCellValue();
//            System.out.println("name : " + name);
//        }
//
//        if (row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//            Date birthdate = row.getCell(1).getDateCellValue();
//            System.out.println("birthdate :" + birthdate);
//        }

        myExcelBook.close();

    }

    @SuppressWarnings("deprecation")
    public static void writeIntoExcel(String file) throws FileNotFoundException, IOException {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Birthdays");

        // ��������� ���������� � ����
        Row row = sheet.createRow(0);

        // �� ������� ��� � ���� � ��� �������
        // ��� ����� String, � ���� �������� --- Date,
        // ������� dd.mm.yyyy
        Cell name = row.createCell(0);
        name.setCellValue("John");

        Cell birthdate = row.createCell(1);

        DataFormat format = book.createDataFormat();
        CellStyle dateStyle = book.createCellStyle();
        dateStyle.setDataFormat(format.getFormat("dd.mm.yyyy"));
        birthdate.setCellStyle(dateStyle);


        // ��������� ��� ���������� � 1900-��
        birthdate.setCellValue(new Date(110, 10, 10));

        // ������ ������ �������
        sheet.autoSizeColumn(1);

        // ���������� �� � ����
        book.write(new FileOutputStream(file));
        book.close();
    }


}
