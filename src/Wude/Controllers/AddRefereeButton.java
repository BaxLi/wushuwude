package Wude.Controllers;

import Wude.Wude;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class AddRefereeButton {
    public TextField lblName;
    public TextField lblFamily;
    public TextField lblPatronymic;

    public void btnCompetitionAtRefereeAddNewViewFormCliked(Event event) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[1]);
    }

    public void btnOkAtRefereeAddNewViewClicked(Event event) {
        String st = null;

        if (lblFamily.getText().isEmpty()) {
            st = "Family ";
        }

        if (lblName.getText().isEmpty()) {
            if (st==null) {st= "Name";}
            else {st+="& Name";}
        }

        if (!st.isEmpty()) {
            AlertingMessages(st);
            return;
        }

        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[3]);
        return;
    }

    public void AlertingMessages(String st) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText(st + " is a mandatory field");
        alert.setContentText("You have to fill it to add a person");
        alert.showAndWait();
    }
}
