package Wude.Controllers;

import Wude.Objects.Participant;
import Wude.Wude;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TestCheckCompetitionValues implements Initializable {

    public TextField shortCompetitionNameFX;
    public TextField longCompetitionNameFX;
    public TextField startfromDate;
    public TextField endToDate;

    public void btnCompetitionClicked(Event event) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        shortCompetitionNameFX.setText(Wude.newCompetitionSetup.getCompetitionShortName());
        longCompetitionNameFX.setText(Wude.newCompetitionSetup.getCompetitionFullName());
        startfromDate.setText(String.valueOf(Wude.newCompetitionSetup.getStartCompetitionDate()));
        endToDate.setText(String.valueOf(Wude.newCompetitionSetup.getEndCompetitionDate()));
    }

    public void demoShow() {
        shortCompetitionNameFX.setText(Wude.newCompetitionSetup.getCompetitionShortName());
        longCompetitionNameFX.setText(Wude.newCompetitionSetup.getCompetitionFullName());
        startfromDate.setText(String.valueOf(Wude.newCompetitionSetup.getStartCompetitionDate()));
        endToDate.setText(String.valueOf(Wude.newCompetitionSetup.getEndCompetitionDate()));

    }

    public void btnCompetitionPressed(ActionEvent actionEvent) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[1]);
    }

    public void refreshBttnPressed(ActionEvent actionEvent) {
        demoShow();
    }

    public void generatePDFButtonPressed(ActionEvent actionEvent) {
        String fileName = "d:/Competition_Process.pdf";
        File file = new File(fileName);
        file.getParentFile().mkdirs();
        try {
            createPdf(fileName);
            JOptionPane.showMessageDialog(null,"File created successfully at "+fileName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        /*
        * help is here - http://developers.itextpdf.com/examples/itext-building-blocks/chapter-and-section-examples
        * */
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
        Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
        Chunk chunk = new Chunk("Competition document ", chapterFont);
        Chapter chapter = new Chapter(new Paragraph(chunk), 1);
        chapter.setNumberDepth(0);
        chapter.add(new Paragraph("This is the paragraph", paragraphFont));
        document.add(chapter);
        document.add(new LineSeparator());
        Paragraph tempParagraph=new Paragraph(Wude.newCompetitionSetup.getCompetitionFullName());
        tempParagraph.add(new Chunk().NEWLINE);
       tempParagraph.add(Wude.newCompetitionSetup.getCompetitionShortName());
        tempParagraph.add(new Chunk().NEWLINE);
        PdfPTable table = new PdfPTable(3);
        int i=0;
        for (Participant participant:Wude.newCompetitionSetup.participantsObservableList)
        {
            table.addCell(String.valueOf(i++));
            table.addCell(participant.getName() + " " + participant.getFamily() + "->");
            table.addCell(String.valueOf(participant.getDlinnoeOrujieSelected()));
        }
        document.add(tempParagraph);
        document.add(table);
        document.close();
    }

}
