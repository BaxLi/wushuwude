package Wude.Controllers;

import Wude.Objects.*;
import Wude.Wude;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.util.Callback;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CompetitionWizardController implements Initializable {

    public ImageView logoImageAtCompetitionWizard;
    public DatePicker startCompetitionDatePicker = new DatePicker();
    public DatePicker endCompetitionDatePicker = new DatePicker();
    public TabPane tabPaneAtCompetitionWizard;
    public ChoiceBox<Referee> choiceBoxGeneralRefereeSelection;
    public ChoiceBox<Referee> choiseboxGeneralSecretarySelection;
    public TableView newCompetitionRefereeTable;
    public ChoiceBox<Associations> associationSelecttionChoiseBox;
    public ChoiceBox<RefereeCategories> refereeCategorySelectionChoiseBox;
    public TextField newRefereeName;
    public TextField newRefereeFamily;
    public ChoiceBox participantTeamSelector;
    public RadioButton radioButton1;
    public RadioButton radioButton2;
    public RadioButton radioButton3;
    public ListView listAReferees;
    public ListView listBReferees;
    public ToggleButton toggleButtonMat;
    public TextField shortNameOfTheCompetition;
    public TextField fullNameOfTheCompetition;
    public ListView listCReferees;
    public ChoiceBox carpet1ChiefRefereeSelection;
    public TableView participantsDBTable;
    public TableView participantsCompetitionTypes;
    public TableView selectedParticipantsListTable;
    public ListView dlinnoeOrujieListViewTable;

    public CheckBox dlinnoeOrujieCheckBox;
    public ListView kulakListViewTable;
    public ListView korotkoeOrujieListViewTable;
    public CheckBox korotkoeOrujieCheckBox;
    public CheckBox kulakCheckBox;
    public Button HideDBButton;
    public Label listOfParticipantDBLabel;
    public Label TemporaryLabelCarpetsQty;

    ObservableList<Referee> carpetChiefRefereeSelection = FXCollections.observableArrayList(new ArrayList<>());
    ObservableList<Referee> refereesCollection = Wude.newCompetitionSetup.refereesObservableList;
    ObservableList<Associations> associationsObservableList = Wude.newCompetitionSetup.associationsObservableList;
    ObservableList<RefereeCategories> refereeCategories = Wude.newCompetitionSetup.refereeCategoriesObservableList;
    ObservableList<Team> teamsObservableList = Wude.newCompetitionSetup.teamsObservableList;
    ObservableList<Participant> selectedParticipantsObservableList = FXCollections.observableArrayList(new ArrayList<>());
    ObservableList<String> dlinnoeOrujieList = Wude.newCompetitionSetup.dlinnoeOrujieList;
    ObservableList<String> korotkoeOrujieList = Wude.newCompetitionSetup.korotkoeOrujieList;
    ObservableList<String> kulakList = Wude.newCompetitionSetup.kulakList;
    Participant currentSelectedParticipantFromListTable = null;
    private boolean selectedAnotherPerson = false;

    /**
     * recreates list of referees of category C eliminating from All referies already selected in A&B catogories
     */

    private void carpetChiefRefereeSelection(ObservableList<Referee> add1, ObservableList<Referee> add2) {
        carpetChiefRefereeSelection.setAll(add1);
        for (Referee rf : add2) {
            if (carpetChiefRefereeSelection.indexOf(rf) < 0)
                carpetChiefRefereeSelection.add(rf);

        }
    }

    private void listCRefereesRefresh() {
        ArrayList<Referee> tmpX = new ArrayList<Referee>(refereesCollection);
        ObservableList<Referee> tmp = FXCollections.observableArrayList(tmpX);
        ObservableList<Referee> bTmp = listBReferees.getSelectionModel().getSelectedItems();
        ObservableList<Referee> aTmp = listAReferees.getSelectionModel().getSelectedItems();

        for (Referee rf : bTmp) {
            if (tmp.indexOf(rf) >= 0)
                tmp.remove(rf);
        }
        for (Referee rf : aTmp) {
            if (tmp.indexOf(rf) >= 0)
                tmp.remove(rf);
        }
        listCReferees.setItems(FXCollections.observableArrayList(tmp));
        listCReferees.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    public void initialize(URL location, ResourceBundle resources) {
//        Main view + Tab 1 initialization
        radioButton1.setSelected(true);
        radioButton2.setSelected(false);
        radioButton3.setSelected(false);

        //todo
        // line to be removed after testing !!!
        TemporaryLabelCarpetsQty.setText(TemporaryLabelCarpetsQty.getText()+" "+Wude.newCompetitionSetup.getColicestvoKovrorvSorevnovanii());

        carpet1ChiefRefereeSelection.setItems(carpetChiefRefereeSelection);
        carpet1ChiefRefereeSelection.getSelectionModel().select(0);

        logoImageAtCompetitionWizard.setImage(Wude.logoImagePic);

        startCompetitionDatePicker.setValue(LocalDate.now());
        startCompetitionDatePicker.setOnAction(event -> {
            Wude.newCompetitionSetup.setStartCompetitionDate(startCompetitionDatePicker.getValue());
            endCompetitionDatePicker.setValue(startCompetitionDatePicker.getValue().plusDays(0));
        });

        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(startCompetitionDatePicker.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #EEEEEE;");
                        }
                    }
                };
            }
        };
        endCompetitionDatePicker.setDayCellFactory(dayCellFactory);
        endCompetitionDatePicker.setValue(startCompetitionDatePicker.getValue().plusDays(0)); // +0 - Just to store code for the future usage
        endCompetitionDatePicker.setOnAction(event -> {
            Wude.newCompetitionSetup.setEndCompetitionDate(endCompetitionDatePicker.getValue());
        });

//     Tab 2 - Referee  initialization

        choiceBoxGeneralRefereeSelection.setItems(refereesCollection);

        choiceBoxGeneralRefereeSelection.getSelectionModel().select(1);
        Wude.newCompetitionSetup.setCompetitionGeneralReferee(choiceBoxGeneralRefereeSelection.getSelectionModel().getSelectedItem());
        choiseboxGeneralSecretarySelection.setItems(refereesCollection);
        choiseboxGeneralSecretarySelection.getSelectionModel().select(2);
        Wude.newCompetitionSetup.setCompetitionGeneralSecretary(choiseboxGeneralSecretarySelection.getSelectionModel().getSelectedItem());
        listCReferees.setItems(null);
        listAReferees.setItems(refereesCollection);
        listAReferees.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listAReferees.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                Wude.newCompetitionSetup.listAselectedReferees = listAReferees.getSelectionModel().getSelectedItems();
                listCRefereesRefresh();
                carpetChiefRefereeSelection(listAReferees.getSelectionModel().getSelectedItems(), listBReferees.getSelectionModel().getSelectedItems());
            }
        });

        listBReferees.setItems(refereesCollection);
        listBReferees.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listBReferees.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                Wude.newCompetitionSetup.listAselectedReferees = listAReferees.getSelectionModel().getSelectedItems();
                listCRefereesRefresh();
                carpetChiefRefereeSelection(listAReferees.getSelectionModel().getSelectedItems(), listBReferees.getSelectionModel().getSelectedItems());
            }
        });

        associationSelecttionChoiseBox.setItems(associationsObservableList);
        participantTeamSelector.setItems(teamsObservableList);

        associationSelecttionChoiseBox.getSelectionModel().select(1);
        participantTeamSelector.getSelectionModel().select(1);

        associationSelecttionChoiseBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                Wude.newCompetitionSetup.listBselectedReferees = listBReferees.getSelectionModel().getSelectedItems();
                listCRefereesRefresh();
                carpetChiefRefereeSelection(listAReferees.getSelectionModel().getSelectedItems(), listBReferees.getSelectionModel().getSelectedItems());
            }
        });

        participantTeamSelector.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Associations>() {
            @Override
            public void changed(ObservableValue<? extends Associations> observable, Associations oldValue, Associations newValue) {
                if (newValue.getAssociationID() == 99) {
                    int index = 0;
                    for (Associations tmp : associationsObservableList) {
                        if ((tmp.getAssociationID() > index) && (tmp.getAssociationID() != 99))
                            index = tmp.getAssociationID();
                    }
                    associationsObservableList.add(index + 1, new Associations(index + 1, "hrrr"));
                    associationSelecttionChoiseBox.getSelectionModel().select(index + 1);
                }
            }
        });

        refereeCategorySelectionChoiseBox.setItems(refereeCategories);
        refereeCategorySelectionChoiseBox.getSelectionModel().select(1);

        refereeCategorySelectionChoiseBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RefereeCategories>() {
            @Override
            public void changed(ObservableValue<? extends RefereeCategories> observable, RefereeCategories oldValue, RefereeCategories newValue) {
                if (newValue.getRefereeCategoryID() == 99) {
                    int index = 0;
                    for (RefereeCategories tmp : refereeCategories) {
                        if ((tmp.getRefereeCategoryID() > index) && (tmp.getRefereeCategoryID() != 99))
                            index = tmp.getRefereeCategoryID();
                    }
                    refereeCategories.add(index + 1, new RefereeCategories(index + 1, "hrrr"));
                    refereeCategorySelectionChoiseBox.getSelectionModel().select(index + 1);
                }
            }
        });

/////  ------------------------          --------- Referee table

        newCompetitionRefereeTable.setItems(refereesCollection);

        if (newCompetitionRefereeTable.getColumns().size() == 0) {
            newCompetitionRefereeTable.setEditable(true);
            newCompetitionRefereeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        }

        TableColumn<Referee, Integer> idCol = new TableColumn<Referee, Integer>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("personID"));

        TableColumn<Referee, String> nameCol = new TableColumn<Referee, String>("Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Referee, String> familyNameCol = new TableColumn<Referee, String>("Family");
        familyNameCol.setCellValueFactory(new PropertyValueFactory<>("family"));

        TableColumn<Referee, Integer> categoryNameCol = new TableColumn<Referee, Integer>("Category");
        categoryNameCol.setCellValueFactory(new PropertyValueFactory<>("category"));

//------------------------ show association by ID ------------------
        TableColumn<Referee, String> associationNameCol = new TableColumn<>("Association");

        associationNameCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Referee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Referee, String> param) {
                return new SimpleStringProperty(associationsObservableList.get(param.getValue().getAssociation()).getAssociationName());
            }
        });
//---------------------------------------------------------------------
        TableColumn<Referee, Boolean> selectedNameCol = new TableColumn<Referee, Boolean>("Selected");
        selectedNameCol.setCellValueFactory(new PropertyValueFactory<>("selectedForCurrentCompetition"));


        newCompetitionRefereeTable.getColumns().addAll(idCol, nameCol, familyNameCol, categoryNameCol, associationNameCol, selectedNameCol);

        shortNameOfTheCompetition.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                Wude.newCompetitionSetup.setCompetitionShortName(newValue);
            }
        });
        fullNameOfTheCompetition.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                Wude.newCompetitionSetup.setCompetitionFullName(newValue);
            }
        });

/////   TAB 3         --------- Participants DB table
        {
            ParticipantDBTableCreate.ParticipantTableCreate(participantsDBTable);
        }

        /////            --------- Competicions type selection table

        ObservableList<Associations> newassociationsObservableList = FXCollections.observableArrayList(Wude.newCompetitionSetup.associationsObservableList);
        newassociationsObservableList.remove(0);

//---------------participants selection from DB

        participantsDBTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && (event.getClickCount() == 2)) {
                    try {
                        ObservableList<Participant> selectedParticipantsItems =
                                participantsDBTable.getSelectionModel().getSelectedItems();
                        for (Participant pr : selectedParticipantsItems) {
                            if (!selectedParticipantsObservableList.contains(pr)) {
                                selectedParticipantsObservableList.add(pr);
                            }
                        }
                    } catch (Exception ex) {
                    }
                }
            }
        });

        // ----------------- Selected participans table

        SelectedParticipantsTable.SelectedParticipantsTable(selectedParticipantsListTable, selectedParticipantsObservableList);

        selectedParticipantsListTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && (event.getClickCount() == 2)) {
                    try {
                        ObservableList<Participant> selectedParticipantsItems =
                                selectedParticipantsListTable.getSelectionModel().getSelectedItems();
                        for (Participant pr : selectedParticipantsItems) {
                            if (selectedParticipantsObservableList.contains(pr)) {
                                selectedParticipantsObservableList.remove(pr);
                            }
                        }
                    } catch (Exception ex) {
                    }
                }
                if (event.isPrimaryButtonDown() || (event.getClickCount() == 1)) {
                    Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                    dlinnoeOrujieCheckBox.setSelected(tmpParticipant.getDlinnoeOrujieSelected());
                    dlinnoeOrujieListViewTable.getSelectionModel().clearSelection();
                    if (tmpParticipant.getDlinnoeOrujieSelected()) {
                        for (String dlinnoeOrujieSubType : tmpParticipant.getDlinnoeOrujieList()) {
                            dlinnoeOrujieListViewTable.getSelectionModel().select(dlinnoeOrujieSubType);
                        }
                    }
                    korotkoeOrujieCheckBox.setSelected(tmpParticipant.getKorotkoeOrujieSelected());
                    korotkoeOrujieListViewTable.getSelectionModel().clearSelection();
                    if (tmpParticipant.getKorotkoeOrujieSelected()) {
                        for (String korotkoeOrujieSubType : tmpParticipant.getKorotkoeOrujieList()) {
                            korotkoeOrujieListViewTable.getSelectionModel().select(korotkoeOrujieSubType);
                        }
                    }
                    kulakCheckBox.setSelected(tmpParticipant.getKulakSelected());
                    kulakListViewTable.getSelectionModel().clearSelection();
                    if (tmpParticipant.getKulakSelected()) {
                        for (String kulakSubType : tmpParticipant.getKulakList()) {
                            kulakListViewTable.getSelectionModel().select(kulakSubType);
                        }
                    }
                    currentSelectedParticipantFromListTable = tmpParticipant;
                }
            }
        });

        //--------------------Initialization ------------

        OrujieListViewTableInit.OrujieListViewTableInit(dlinnoeOrujieListViewTable, dlinnoeOrujieList);
        OrujieListViewTableInit.OrujieListViewTableInit(korotkoeOrujieListViewTable, korotkoeOrujieList);
        OrujieListViewTableInit.OrujieListViewTableInit(kulakListViewTable, kulakList);
//-1----------------------------------------------------------------------------------------------------------------------------------
        dlinnoeOrujieCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if (newValue == true) {
                    if (tmpParticipant != null) tmpParticipant.setDlinnoeOrujieSelected(true);
                } else {
                    if (tmpParticipant != null) tmpParticipant.setDlinnoeOrujieSelected(false);
                }
            }
        });
        dlinnoeOrujieListViewTable.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if ((tmpParticipant.getDlinnoeOrujieSelected()) & (tmpParticipant.equals(currentSelectedParticipantFromListTable))) {
                    tmpParticipant.setDlinnoeOrujieList(dlinnoeOrujieListViewTable.getSelectionModel().getSelectedItems());
                }
            }
        });
//-2----------------------------------------------------------------------------------------------------------------------------------
        korotkoeOrujieCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if (newValue == true) {
                    if (tmpParticipant != null) tmpParticipant.setKorotkoeOrujieSelected(true);
                } else {
                    if (tmpParticipant != null) tmpParticipant.setKorotkoeOrujieSelected(false);
                }
            }
        });
        korotkoeOrujieListViewTable.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if ((tmpParticipant.getKorotkoeOrujieSelected()) & (tmpParticipant.equals(currentSelectedParticipantFromListTable))) {
                    tmpParticipant.setKorotkoeOrujieList(korotkoeOrujieListViewTable.getSelectionModel().getSelectedItems());
                }
            }
        });
//------------------------------------------------------------------------------------------------------------------------------------
        kulakCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if (newValue == true) {
                    kulakListViewTable.setVisible(true);
                    if (tmpParticipant != null) tmpParticipant.setKulakSelected(true);
                } else {
                    if (tmpParticipant != null) tmpParticipant.setKulakSelected(false);
                }
            }
        });

        kulakListViewTable.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                Participant tmpParticipant = (Participant) selectedParticipantsListTable.getSelectionModel().getSelectedItem();
                if ((tmpParticipant.getKulakSelected()) & (tmpParticipant.equals(currentSelectedParticipantFromListTable))) {
                    tmpParticipant.setKulakList(kulakListViewTable.getSelectionModel().getSelectedItems());
                }
            }
        });
//------------------------------------------------------------------------------------------------------------------------------------


    }

    public void btnCompetitionatCompetitionWizardCliked(Event event) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[1]);
    }

    public void btnRefereeAtCompetitionWizardCliked(Event event) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[2]);

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Was Added");
        alert.setContentText("selected create new item.");
        alert.showAndWait();
    }

    public void btnNextAtCompetitionWizardDatesPageCliked(Event event) {
        tabPaneAtCompetitionWizard.getSelectionModel().selectNext();
    }

    public void btnRefereeScrolled(Event event) {
        System.out.println(choiceBoxGeneralRefereeSelection);
    }

    public void btnchoiceBoxGeneralRefereeSelectionCliked(Event event) {
        System.out.println(choiceBoxGeneralRefereeSelection);
    }

    public void associationChoiseBoxChanged(Event event) {
    }

    public void addNewRefereeBtnCliked(ActionEvent actionEvent) {
        int index = 0;
        for (Referee tmp : refereesCollection) {
            if ((tmp.getPersonID() > index) && (tmp.getPersonID() != 99)) index = tmp.getPersonID();
        }
        Referee newReferee = new Referee(index + 1, newRefereeName.getText(), newRefereeFamily.getText(), "", "", associationSelecttionChoiseBox.getSelectionModel().getSelectedIndex(), refereeCategorySelectionChoiseBox.getSelectionModel().getSelectedIndex(), true);
        refereesCollection.add(refereesCollection.size(), newReferee);
    }

    public void radiobuttton1selected(ActionEvent actionEvent) {
        if (radioButton1.isSelected()) {
            Wude.newCompetitionSetup.setColicestvoKovrorvSorevnovanii((byte) 1);
            radioButton2.setSelected(false);
            radioButton3.setSelected(false);
        }
        //todo
        // line to be removed after testing !!!
        TemporaryLabelCarpetsQty.setText("Carpets="+Wude.newCompetitionSetup.getColicestvoKovrorvSorevnovanii());
    }

    public void radioButton2Selected(ActionEvent actionEvent) {
        if (radioButton2.isSelected()) {
            Wude.newCompetitionSetup.setColicestvoKovrorvSorevnovanii((byte) 2);
            radioButton1.setSelected(false);
            radioButton3.setSelected(false);
        }
        //todo
        // line to be removed after testing !!!
        TemporaryLabelCarpetsQty.setText("Carpets="+Wude.newCompetitionSetup.getColicestvoKovrorvSorevnovanii());
    }

    public void radioButton3Selected(ActionEvent actionEvent) {
        if (radioButton3.isSelected()) {
            Wude.newCompetitionSetup.setColicestvoKovrorvSorevnovanii((byte) 3);
            radioButton1.setSelected(false);
            radioButton2.setSelected(false);
        }
        //todo
        // line to be removed after testing !!!
        TemporaryLabelCarpetsQty.setText("Carpets="+Wude.newCompetitionSetup.getColicestvoKovrorvSorevnovanii());
    }

    public void onSelectedParticipantKeyPressed(Event event) {
    }

    public void HIdeDBTableButtonPressed(ActionEvent actionEvent) {
       if (HideDBButton.getText()== "Unhide DB"){
           participantsDBTable.setMaxWidth(Region.USE_COMPUTED_SIZE);
           HideDBButton.setText("Hide DB");
           listOfParticipantDBLabel.setVisible(true);
       }
           else
       {
        participantsDBTable.setMaxWidth(5.0);
        HideDBButton.setText("Unhide DB");
           listOfParticipantDBLabel.setVisible(false);}

    }
}
