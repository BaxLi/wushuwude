package Wude.Controllers;

import Wude.Wude;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MainPageController implements Initializable {
    @FXML
    public BorderPane mainBorderPane;
    public Label mnLabelInfoToShow;
    public Button btnShowRefereeTableView;
    public ImageView logoImageonWudePage = new ImageView();
    public Button btnCurrentCompetition;
    public Stage stageRefereeTabbleView = new Stage();
    private int i = 0;
    private ActionEvent actionEvent;

    public MainPageController() {
    }

    @Override

    public void initialize(URL location, ResourceBundle resources) {

        logoImageonWudePage.setImage(Wude.logoImagePic);
        if (!Wude.newCompetitionSetup.getIsCompetitionGoing())
            btnCurrentCompetition.setStyle("-fx-background-color: lightgrey;");

        Image bckImg = new Image(Wude.class.getResourceAsStream("Pictures/background.jpg"));
        BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(bckImg, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER, backgroundSize);
        mainBorderPane.setBackground(new Background(backgroundImage));
        TestCheckCompetitionValues demoCheck = new TestCheckCompetitionValues();
    }

    public void btnRefereeTableViewCliked(Event actionEvent) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[2]);
    }

    public void btnCompetitionClicked(Event event) {

    }

    public void btnMainNextClicked(Event event) {

    }

    public void btnStartNewCompeitionCliked(Event event) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[3]);
    }

    public void btnCurrentCompetitionCliked(Event event) {
        if (Wude.newCompetitionSetup.getIsCompetitionGoing()) Wude.mainWindowsStage.setScene(Wude.scenesArrayList[4]);
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Look, the competition first should start!");
            alert.setContentText("Now it is impossible to step in.");
            alert.showAndWait();
        }
    }

    public void defaultBtnPressed(ActionEvent actionEvent) {

        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[50]);

    }
}
