package Wude.Controllers;

import Wude.Objects.Referee;
import Wude.Wude;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class RefereeTableViewController implements Initializable {
@FXML
    public ImageView logoImageatRefereeTableView;
    public VBox vboxRefereTableView;
    public Button btnRefereeTableView;

    public TableView<Referee> refereeTableView;
    public ObservableList<Referee> refereeCollection = FXCollections.observableArrayList();
    public ListView<Referee> selectedRefereesListAtRefereeTableView;
    public VBox selectedRefereeVboxAtRefereeTableView;

    public Button btnCompetitionatRefereeTableView;

    public RefereeTableViewController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logoImageatRefereeTableView.setImage(Wude.logoImagePic);
/* initialize table view of referees
@param refereeTableView
@param
 */

        if (refereeTableView.getColumns().size() == 0) {
            refereeTableView.setEditable(true);
            refereeTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            TableColumn<Referee, Integer> idCol = new TableColumn<Referee, Integer>("ID");
            idCol.setCellValueFactory(new PropertyValueFactory<Referee, Integer>("personID"));

            TableColumn<Referee, String> nameCol = new TableColumn<Referee, String>("Name");
            nameCol.setCellValueFactory(new PropertyValueFactory<Referee, String>("name"));

            TableColumn<Referee, String> familyNameCol = new TableColumn<Referee, String>("Family");
            familyNameCol.setCellValueFactory(new PropertyValueFactory<Referee, String>("family"));

            TableColumn<Referee, String> levelNameCol = new TableColumn<Referee, String>("Level");

            refereeTableView.getColumns().addAll(idCol, nameCol, familyNameCol, levelNameCol);
        }
    }

    public void listViewMouseCliked(MouseEvent event) {
        System.out.println(event.getEventType());
        if (event.getClickCount() == 2) {
            refereeTableView.getSelectionModel().getSelectedCells();
            System.out.println("Double mouse cliked" + selectedRefereesListAtRefereeTableView.getSelectionModel().getSelectedItem().getFamily());
        }
    }

    public void btnCompetitionatRefereeTableViewCliked(Event event) {

        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[1]);
    }
}

