package Wude.Controllers;

import Wude.Wude;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.grios.jwebcam_simple.fx.FXWebCamSimple;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by BaxLi on 7/30/2015.
 */
public class WebCameraViewController implements Initializable {

    public GridPane topPane;
    public Button btnStartDevice;
    public Button btnStopDevice;
    public BorderPane mainPane = new BorderPane();
    public ComboBox<String> cmbVideoDevices= new ComboBox<String>(FXCollections.observableArrayList());
    public ImageView imgvDisplay;

    FXWebCamSimple fxWebCam = new FXWebCamSimple();


//    Button btnStopDevice;

    public void btnRefereeAtCompetitionWizardCliked(Event event) {


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgvDisplay.setImage(Wude.logoImagePic);
//        imgvDisplay.setVisible(true);
        imgvDisplay.setSmooth(true);
        imgvDisplay.setCache(true);
        fillComboBox();
//       imgvDisplay.fitWidthProperty().bind(mainPane.widthProperty());
//       imgvDisplay.fitHeightProperty().bind(mainPane.heightProperty());
    }

    private void fillComboBox() {
        try {
            cmbVideoDevices.getItems().clear();
            for (String str : FXWebCamSimple.getDeviceNames()) {
                cmbVideoDevices.getItems().add(str);
            }
//            if (cmbVideoDevices.getItems().size() == 0)
                cmbVideoDevices.getSelectionModel().selectFirst();
        } catch (Exception e) {
//            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Can't initialize ANY web-camera - please take care!");
            alert.setContentText("Now it is impossible to capture video.");
            alert.showAndWait();
        }
    }

    public void btnCompetitionatWebCam(Event event) {
        Wude.mainWindowsStage.setScene(Wude.scenesArrayList[1]);
    }

    public void btnStartDeviceCliked(Event event) {
        try {
            btnStartDevice.setDisable(true);
            cmbVideoDevices.setDisable(true);

            fxWebCam.start(cmbVideoDevices.getSelectionModel().getSelectedItem(), imgvDisplay);
        } catch (Exception e) {
//            e.printStackTrace();
//            System.exit(0);
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Can't START web-camera");
            alert.setContentText("It is impossible to capture video.");
            alert.showAndWait();
        }
    }

    public void btnStopDeviceCliked(Event event) {

        try {
            fxWebCam.stop();
            btnStartDevice.setDisable(false);
            cmbVideoDevices.setDisable(false);
        } catch (Exception e) {
//            e.printStackTrace();
//            System.exit(0);
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Can't STOP web-camera - please take care!");
            alert.setContentText("Capture video continue.");
            alert.showAndWait();
        }

    }
}
