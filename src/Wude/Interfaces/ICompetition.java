package Wude.Interfaces;

import Wude.Objects.Referee;

import java.time.LocalDate;


public interface ICompetition {


    public String setCompetitionFullName(String a); //Full name of competition

    public String getCompetitionFullName();

    public String setPrintCompetitionFullName(String a); //Category of competition

    public String getPrintCompetitionFullName();

    public LocalDate setStartCompetitionDate(LocalDate a);

    public LocalDate getStartCompetitionDate();

    public LocalDate setEndCompetitionDate(LocalDate a);

    public LocalDate getEndCompetitionDate();

    public Referee setCompetitionGeneralReferee(Referee a);

    public Referee getCompetitionGeneralReferee();

    public Referee setCompetitionGeneralSecretary(Referee a);

    public Referee getCompetitionGeneralSecretary();

    public Boolean setIsCompetitionGoing(Boolean a);

    public Boolean getIsCompetitionGoing();



}
