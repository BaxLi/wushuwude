package Wude.Interfaces;

/**
 * Created by BaxWorkPc on 10.07.2015.
 */
public interface IPerson {

    public Integer getPersonID();
    public Integer setPersonID(Integer i);

    public String getIDNP();
    public String setIDNP(String n);

    public String getName();
    public String setName(String n);

    public String getFamily();
    public String setFamily(String f);

    public String getPatronymic();
    public String setPatronymic(String p);

    public String getBirthday();
    public String setBirthday(String d);

    public Boolean setGender(Boolean a); // Gender male is True / femaile is False
    public Boolean getGender();

    public String getPhoto();
    public String setPhoto(String photoFileName);

    public Boolean getSelectedForCurrentCompetition();
    public Boolean setSelectedForCurrentCompetition(Boolean b);

    public String getPostalAddress();
    public String setPostalAddress(String address);




}
