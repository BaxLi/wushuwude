package Wude.Interfaces;

/**
 * Created by BaxLi on 08-Jul-15.
 */

public interface IReferee extends IPerson  {

    public int getCategory();
    public int setCategory(int i);

    public int getAssociation(); //Which sport association/federation is this person
    public int setAssociation(int i);

    public String getLogin();
    public String setLogin(String n);

    public String getPassword();
    public String setPassword(String n);

    public String getTeam();
    public String setTeam(String photoFileName);

}
