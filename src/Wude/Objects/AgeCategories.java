package Wude.Objects;

public class AgeCategories {
    private Integer categoryID;
    private String agingName;
    private Integer lowerAge;
    private Integer hierAge;

    public AgeCategories(Integer categoryID, String agingName, Integer lowerAge, Integer hierAge) {
        this.categoryID = categoryID;
        this.agingName = agingName;
        this.lowerAge = lowerAge;
        this.hierAge = hierAge;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getAgingName() {
        return agingName;
    }

    public void setAgingName(String agingName) {
        this.agingName = agingName;
    }

    public Integer getLowerAge() {
        return lowerAge;
    }

    public void setLowerAge(Integer lowerAge) {
        this.lowerAge = lowerAge;
    }

    public Integer getHierAge() {
        return hierAge;
    }

    public void setHierAge(Integer hierAge) {
        this.hierAge = hierAge;
    }
}
