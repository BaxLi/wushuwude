package Wude.Objects;

import Wude.Interfaces.IParticipant;
import Wude.Objects.AgingSelectionAlgorythm.AgingSelectByCurrentDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Participant implements IParticipant {
    private Integer personID;
    private String team;

    private Integer teamID;
    private String name;
    private String family;
    private String patronymic;
    private String participantAgeCategory;
    private Boolean gender; // male - true - female - false
    private String birthday;
    private String club;
    private String razread;
    private String postalAddress;
    private String idnp;
    private String phoneNr;
    private String sportPassportNr;
    private String email;
    private String coachID;

    private ObservableList<String> dlinnoeOrujieList;
    private boolean dlinnoeOrujieSelected;
    private ObservableList<String> korotkoeOrujieList;
    private boolean korotkoeOrujieSelected;
    private ObservableList<String> kulakList;
    private boolean kulakSelected;

    public Participant(Integer id, String name, String family, String patronymic, String birth, Integer team, String idnpPerson) {
        this.personID = id;
        this.teamID =team;
        this.team =team.toString();
        this.name = name;
        this.family = family;
        this.patronymic = patronymic;
        birthday = birth;
        idnp=idnpPerson;
        setParticipantAgeCategory(AgingSelectByCurrentDate.ageSelectedByCurrentDate(birth));
        dlinnoeOrujieList=FXCollections.observableArrayList(new ArrayList<>());
        dlinnoeOrujieSelected=false;
        korotkoeOrujieList= FXCollections.observableArrayList(new ArrayList<>());
        korotkoeOrujieSelected=false;
        kulakList=FXCollections.observableArrayList(new ArrayList<>());
        kulakSelected=false;
    }

    public ObservableList<String> getKorotkoeOrujieList() {
        return korotkoeOrujieList;
    }

    public void setKorotkoeOrujieList(ObservableList<String> korotkoeOrujieListSelected) {
        korotkoeOrujieList.clear();
        for (String tmpVar:korotkoeOrujieListSelected){
            korotkoeOrujieList.add(tmpVar);}
    }

    public ObservableList<String> getKulakList() {
        return kulakList;
    }

    public void setKulakList(ObservableList<String> kulakListSelected) {
        kulakList.clear();
        for (String tmpVar:kulakListSelected){
            kulakList.add(tmpVar);}
    }

    public boolean getKorotkoeOrujieSelected() {
        return korotkoeOrujieSelected;
    }

    public void setKorotkoeOrujieSelected(boolean korotkoeOrujieSelected) {
        this.korotkoeOrujieSelected = korotkoeOrujieSelected;
    }

    public boolean getKulakSelected() {
        return kulakSelected;
    }

    public void setKulakSelected(boolean kulakSelected) {
        this.kulakSelected = kulakSelected;
    }

    public boolean getDlinnoeOrujieSelected() {
        return dlinnoeOrujieSelected;
    }

    public void setDlinnoeOrujieSelected(boolean dlinnoeOrujieSelected) {
        this.dlinnoeOrujieSelected = dlinnoeOrujieSelected;
    }

    public ObservableList<String> getDlinnoeOrujieList() {
        return dlinnoeOrujieList;
    }

    public void setDlinnoeOrujieList(ObservableList<String> dlinnoeOrujieListSelected) {
        dlinnoeOrujieList.clear();
        for (String tmpVar:dlinnoeOrujieListSelected){
        dlinnoeOrujieList.add(tmpVar);
        }
    }

    public Integer getTeamID() {
        return teamID;
    }

    @Override
    public String setTeam(String a) {
        return team = a;
    }

    @Override
    public String getTeam() {
        return team;
    }

    @Override
    public String setviborVidovVistupleniiUceastnikov(Integer[] a) {
        return null;
    }

    @Override
    public String getviborVidovVistupleniiUceastnikov() {
        return null;
    }

    @Override
    public String getParticipantAgeCategory() {
        return this.participantAgeCategory;
    }

    @Override
    public void setParticipantAgeCategory(String st) {
        this.participantAgeCategory = st;
    }

    @Override
    public Integer getPersonID() {
        return personID;
    }

    @Override
    public Integer setPersonID(Integer i) {
        return personID = i;
    }

    @Override
    public String getIDNP() {
        return idnp;
    }

    @Override
    public String setIDNP(String n) {
        return idnp=n;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String setName(String n) {
        return name = n;
    }

    @Override
    public String getFamily() {
        return family;
    }

    @Override
    public String setFamily(String f) {
        return family = f;
    }

    @Override
    public String getPatronymic() {
        return patronymic;
    }

    @Override
    public String setPatronymic(String p) {
        return patronymic = p;
    }

    @Override
    public String getBirthday() {
        return birthday;
    }

    @Override
    public String setBirthday(String d) {
        return birthday = d;
    }

    @Override
    public Boolean setGender(Boolean a) {
        return gender=a;
    }

    @Override
    public Boolean getGender() {
        return gender;
    }

    @Override
    public String getPhoto() {
        return null;
    }

    @Override
    public String setPhoto(String photoFileName) {
        return null;
    }

    @Override
    public Boolean getSelectedForCurrentCompetition() {
        return null;
    }

    @Override
    public Boolean setSelectedForCurrentCompetition(Boolean b) {
        return null;
    }

    @Override
    public String getPostalAddress() {
        return postalAddress;
    }

    @Override
    public String setPostalAddress(String address) {
        return postalAddress=address;
    }


}
