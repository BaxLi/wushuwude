package Wude.Objects;

public class RefereeCategories {
    private int refereeCategoryID;
    private String refereeCategoryName;

    public RefereeCategories(int refereeCategoryID, String refereeCategoryName) {
        this.refereeCategoryID = refereeCategoryID;
        this.refereeCategoryName = refereeCategoryName;
    }

    public int getRefereeCategoryID() {
        return refereeCategoryID;
    }

    public void setRefereeCategoryID(int refereeCategoryID) {
        this.refereeCategoryID = refereeCategoryID;
    }

    public String getRefereeCategoryName() {
        return refereeCategoryName;
    }

    public void setRefereeCategoryName(String refereeCategoryName) {
        this.refereeCategoryName = refereeCategoryName;
    }

    @Override
    public String toString() {
        return refereeCategoryName;
    }
}
