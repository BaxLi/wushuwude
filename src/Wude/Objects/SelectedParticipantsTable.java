package Wude.Objects;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SelectedParticipantsTable {
    public static void SelectedParticipantsTable(TableView selectedParticipantsListTable, ObservableList<Participant> selectedParticipantsObservableList){
        selectedParticipantsListTable.setItems(selectedParticipantsObservableList);

        TableColumn<Participant, Integer> participantId = new TableColumn<>("ID");
        participantId.setCellValueFactory(new PropertyValueFactory<>("personID"));

        TableColumn<Participant, String> participantName = new TableColumn<>("Name");
        participantName.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Participant, String> participantFamily = new TableColumn<>("Family");
        participantFamily.setCellValueFactory(new PropertyValueFactory<>("family"));

        TableColumn<Participant, String> participantBirthday = new TableColumn<>("Birthday");
        participantBirthday.setCellValueFactory(new PropertyValueFactory<>("birthday"));

        TableColumn<Participant, String> participantAging = new TableColumn<>("Aging");
        participantAging.setCellValueFactory(new PropertyValueFactory<>("participantAgeCategory"));

        TableColumn<Participant, String> participantTeam = new TableColumn<>("Team");
        participantTeam.setCellValueFactory(new PropertyValueFactory<>("team"));

        selectedParticipantsListTable.getColumns().addAll(participantId, participantName, participantFamily, participantBirthday, participantAging, participantTeam);


    }
}
