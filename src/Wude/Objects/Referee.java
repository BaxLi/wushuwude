package Wude.Objects;

import Wude.Interfaces.IReferee;

public class Referee implements IReferee {

    int personID = 0;
    String name;
    String family;
    String patronymic;
    String birthday;
    String login;
    String IDNP;
    private String postalAddress;
    int association;
    int category;
    Boolean selectedForCurrentCompetition;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Referee)) return false;
        Referee referee = (Referee) o;
        if (getPersonID() != referee.getPersonID()) return false;
        if (!getName().equals(referee.getName())) return false;
        return getFamily().equals(referee.getFamily());
    }

    @Override
    public int hashCode() {
        int result = getPersonID();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getFamily().hashCode();
        return result;
    }

    public Referee() {
    }

    public Referee(int persID, String nAme, String fAmily, String pAtronymic, String bIrthday, int aSSociation, int cAtegory, Boolean selected) {
        personID = persID;
         name=nAme;
         family=fAmily;
         patronymic=pAtronymic;
         birthday=bIrthday;
         association=aSSociation;
         category=cAtegory;
        selectedForCurrentCompetition=selected;
    }

    @Override
    public String toString() {
        return getFamily() + " " + getName();
    }

    @Override
    public int getCategory() {
        return this.category;
    }

    @Override
    public int setCategory(int i) {
        this.category = i;
        return this.category;
    }

    @Override
    public int getAssociation() {
        return this.association;
    }

    @Override
    public int setAssociation(int i) {
        this.association = i;
        return this.association;
    }

    @Override
    public String getLogin() {
        return this.login;
    }

    @Override
    public String setLogin(String n) {
        this.login = n;
        return this.login;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String setPassword(String n) {
        return null;
    }

    @Override
    public Integer getPersonID() {
        return this.personID;
    }

    @Override
    public Integer setPersonID(Integer i) {
        this.personID = i;
        return this.personID;
    }

    @Override
    public String getIDNP() {
        return IDNP;
    }

    @Override
    public String setIDNP(String n) {
        return null;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String setName(String n) {
        this.name = n;
        return this.name;
    }

    @Override
    public String getFamily() {
        return this.family;
    }

    @Override
    public String setFamily(String f) {
        this.family = f;
        return this.family;
    }

    @Override
    public String getPatronymic() {
        return null;
    }

    @Override
    public String setPatronymic(String p) {
        return null;
    }

    @Override
    public String getBirthday() {
        return null;
    }

    @Override
    public String setBirthday(String d) {
        return null;
    }

    @Override
    public Boolean setGender(Boolean a) {
        return null;
    }

    @Override
    public Boolean getGender() {
        return null;
    }

    @Override
    public String getPhoto() {
        return null;
    }

    @Override
    public String setPhoto(String photoFileName) {
        return null;
    }

    @Override
    public Boolean getSelectedForCurrentCompetition() {
        return selectedForCurrentCompetition;
    }

    @Override
    public Boolean setSelectedForCurrentCompetition(Boolean b) {
        return this.selectedForCurrentCompetition=b;
    }

    @Override
    public String getPostalAddress() {
        return postalAddress;
    }

    @Override
    public String setPostalAddress(String address) {
        return postalAddress=address;
    }

    @Override
    public String getTeam() {
        return null ;
    }

    @Override
    public String setTeam(String photoFileName) {
        return null;
    }

}
