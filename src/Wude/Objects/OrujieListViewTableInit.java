package Wude.Objects;

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

public class OrujieListViewTableInit {

    public static void OrujieListViewTableInit(ListView orujieTableView, ObservableList<String> tipOrujia){
        orujieTableView.setItems(tipOrujia);
        orujieTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        orujieTableView.setVisible(true);
    }

}
