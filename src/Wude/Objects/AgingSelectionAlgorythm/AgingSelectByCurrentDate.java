package Wude.Objects.AgingSelectionAlgorythm;

import javax.swing.*;
import java.time.LocalDate;

public class AgingSelectByCurrentDate {

    public static String ageSelectedByCurrentDate(String birthday) {
        LocalDate todayDate = LocalDate.now();
        String birtpt = birthday;
        String delims = "[-./ ]+";
        String[] tokens = birtpt.split(delims);
        LocalDate ttmmpp = LocalDate.of(Integer.valueOf(tokens[2]), Integer.valueOf(tokens[1]), Integer.valueOf(tokens[0]));
        int diffYears = todayDate.getYear() - ttmmpp.getYear();
        int diffMonth = todayDate.getMonthValue() - ttmmpp.getMonthValue();
        int diffDays = todayDate.getDayOfMonth() - ttmmpp.getDayOfMonth();

        switch (diffYears) {
            case 1:
            case 2:
            case 3:
                JOptionPane.showInputDialog("So young! Please correct the birthday date!");
                return "So young! Please correct the birthday date!";
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return ("till 9 years");
            case 9:
                if (diffMonth < 0) {
                    return ("till 9 years");
                }
                if ((diffDays < 0) & (diffMonth == 0)) {
                    return ("till 9 years");
                } else {
                    return ("Ciutzi (9-12 years)");
                }
            case 10:
            case 11:
            case 12:
                return ("Ciutzi (9-12 years)");
            case 13:
                if (diffMonth < 0) {
                    return ("Ciutzi (9-12 years)");
                }
                if ((diffDays < 0) & (diffMonth == 0)) {
                    return ("Ciutzi (9-12 years)");
                } else {
                    return ("Guidin (13-14 years)");
                }
            case 14:
                return ("Guidin (13-14 years)");
            case 15:
                if (diffMonth < 0) {
                    return ("Guidin (13-14 years)");
                }
                if ((diffDays < 0) & (diffMonth == 0)) {
                    return ("Guidin (13-14 years)");
                } else {
                    return ("Godzea (15-18 years)");
                }
            case 16:
            case 17:
                return ("Godzea (15-18 years)");
            case 18:
                if (diffMonth < 0) {
                    return ("Godzea (15-18 years)");
                }
                if ((diffDays < 0) & (diffMonth == 0)) {
                    return ("Godzea (15-18 years)");
                } else {
                    return ("Old one (18> years)");
                }
            default:
                return ("Old one (18> years)");
        }
//        return "error aging status";
    }
}

