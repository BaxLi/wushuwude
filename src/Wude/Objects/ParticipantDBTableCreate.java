package Wude.Objects;

import Wude.Wude;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ParticipantDBTableCreate {
    public static void ParticipantTableCreate(TableView participantsTable){

        participantsTable.setItems(Wude.newCompetitionSetup.participantsObservableList);

        TableColumn<Participant, Integer> participantId = new TableColumn<>("ID");
        participantId.setCellValueFactory(new PropertyValueFactory<>("personID"));

        TableColumn<Participant, String> participantName = new TableColumn<>("Name");
        participantName.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Participant, String> participantFamily = new TableColumn<>("Family");
        participantFamily.setCellValueFactory(new PropertyValueFactory<>("family"));

        TableColumn<Participant, String> participantBirthday = new TableColumn<>("Birthday");
        participantBirthday.setCellValueFactory(new PropertyValueFactory<>("birthday"));

        TableColumn<Participant, String> participantAging = new TableColumn<>("Aging");
        participantAging.setCellValueFactory(new PropertyValueFactory<>("participantAgeCategory"));

        TableColumn<Participant, String> participantTeam = new TableColumn<>("Team");
        participantTeam.setCellValueFactory(new PropertyValueFactory<>("team"));

        participantsTable.getColumns().addAll(participantId, participantName, participantFamily, participantBirthday, participantAging, participantTeam);
        participantsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }



}
