package Wude.Objects;


import Wude.DBClasses.ReadWriteExcelFile;
import Wude.Interfaces.ICompetition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class CurrentCompetition implements ICompetition, Serializable {

    /**
     * @param isCompetitionGoing - help to understand if in the current moment setup of the parameters for the new competition was totally done
     */
    public ObservableList<Referee> refereesObservableList = FXCollections.observableArrayList(new ArrayList<>());
    public ObservableList<Referee> listAselectedReferees = null;
    public ObservableList<Referee> listBselectedReferees = null;
    public ObservableList<Referee> listCselectedReferees = null;

    public ObservableList<Associations> associationsObservableList = FXCollections.observableArrayList(new ArrayList<>());
    public ObservableList<RefereeCategories> refereeCategoriesObservableList = FXCollections.observableArrayList(new ArrayList<>());
    public ObservableList<Participant> participantsObservableList = FXCollections.observableArrayList(new ArrayList<>());
    public ObservableList<CompetitionSubTapes> participantsCompetitionsTypes = FXCollections.observableArrayList(new ArrayList<>());
    public ObservableList<Referee> carpetsReferees =FXCollections.observableArrayList(new ArrayList<>());
    public static ObservableList<Team> teamsObservableList=FXCollections.observableArrayList(new ArrayList<>());
    public static ObservableList<String> dlinnoeOrujieList = FXCollections.observableArrayList(new ArrayList<>()); // Correct name - Chang qixie
    public static ObservableList<String> korotkoeOrujieList = FXCollections.observableArrayList(new ArrayList<>());// Correct name -  Duan qixie
    public static ObservableList<String> kulakList = FXCollections.observableArrayList(new ArrayList<>());// Correct name - Quan shu

    private String competitionFullName;
    private String competitionShortName;
    private String printCompetitionFullName;

    private LocalDate startCompetitionDate;
    private LocalDate endCompetitionDate;
    private Referee CompetitionGeneralReferee;
    private Referee CompetitionGeneralSecretary;
    private byte colicestvoKovrorvSorevnovanii;

    private boolean isCompetitionGoing;

    public CurrentCompetition() {
        initializeObjectsValues();
    }

    public byte getColicestvoKovrorvSorevnovanii() {
        return colicestvoKovrorvSorevnovanii;
    }

    public void setColicestvoKovrorvSorevnovanii(byte colicestvoKovrorvSorevnovanii) {
        this.colicestvoKovrorvSorevnovanii = colicestvoKovrorvSorevnovanii;
    }

    public String getCompetitionShortName() {
        return competitionShortName;
    }

    public void setCompetitionShortName(String valueShortName) {
        competitionShortName = valueShortName;
    }

    @Override
    public String setCompetitionFullName(String a) {
        try {
            competitionFullName = a;
        } catch (Exception e) {
            competitionFullName = "";
        }
        return competitionFullName;
    }

    @Override
    public String getCompetitionFullName() {
        return this.competitionFullName;
    }

    @Override
    public String setPrintCompetitionFullName(String a) {
        try {
            this.printCompetitionFullName = a;
        } catch (Exception e) {
            this.printCompetitionFullName = "";
        }
        return this.printCompetitionFullName;
    }

    @Override
    public String getPrintCompetitionFullName() {
        return this.printCompetitionFullName;
    }

    @Override
    public LocalDate setStartCompetitionDate(LocalDate a) {
        try {
            this.startCompetitionDate = a;
        } catch (Exception e) {
            this.startCompetitionDate = LocalDate.now();
        }
        return this.startCompetitionDate;
    }

    @Override
    public LocalDate getStartCompetitionDate() {
        return this.startCompetitionDate;
    }

    @Override
    public LocalDate setEndCompetitionDate(LocalDate a) {
        try {
            endCompetitionDate = a;
        } catch (Exception e) {
            endCompetitionDate = LocalDate.now();
        }
        return endCompetitionDate;
    }

    @Override
    public LocalDate getEndCompetitionDate() {
        return endCompetitionDate;
    }

    @Override
    public Referee setCompetitionGeneralReferee(Referee a) {
        CompetitionGeneralReferee = a;
        return CompetitionGeneralReferee;
    }

    @Override
    public Referee getCompetitionGeneralReferee() {
        return CompetitionGeneralReferee;
    }

    @Override
    public Referee setCompetitionGeneralSecretary(Referee a) {
        CompetitionGeneralSecretary = a;
        return this.CompetitionGeneralSecretary;
    }

    @Override
    public Referee getCompetitionGeneralSecretary() {
        return this.CompetitionGeneralSecretary;
    }

    @Override
    public Boolean setIsCompetitionGoing(Boolean a) {
        this.isCompetitionGoing = a;
        return this.isCompetitionGoing;
    }

    @Override
    public Boolean getIsCompetitionGoing() {
        return this.isCompetitionGoing;
    }

    private void readParticipantsFromExcelFile(String fPathAndFName){
    }

    void initializeObjectsValues() {

        isCompetitionGoing = false;
        colicestvoKovrorvSorevnovanii = 1;
        refereesObservableList.add(new Referee(1, "Nicolae", "Mihalache", "Ionovici", "01-01-1980", 1, 1, false));
        refereesObservableList.add(new Referee(2, "Lilian", "Baxan", "Victor", "01-09-1974", 2, 1, false));
        refereesObservableList.add(new Referee(3, "Andrei", "Cataraga", "Zeusovici", "01-01-1985", 3, 1, true));


        // Add associations from excel file
        try {
            ReadWriteExcelFile.readAssociationsFromExcel("C:\\000\\associations.xlsx", associationsObservableList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        associationsObservableList.add(0, new Associations(99, "< add new >"));


// Add participants from excel file
        try {
            ReadWriteExcelFile.readParticipantsFromExcel("C:\\000\\participants.xlsx", participantsObservableList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        refereeCategoriesObservableList.add(new RefereeCategories(1, "Category-1"));
        refereeCategoriesObservableList.add(new RefereeCategories(2, "Category-2"));
        refereeCategoriesObservableList.add(new RefereeCategories(3, "Category-3"));
        refereeCategoriesObservableList.add(0, new RefereeCategories(99, "< add new >"));

        teamsObservableList.add(new Team(1, "team-1"));
        teamsObservableList.add(new Team(2, "team-2"));
        teamsObservableList.add(new Team(3, "team-3"));
        teamsObservableList.add(new Team(4, "team-4"));
        teamsObservableList.add(new Team(5, "team-5"));
        teamsObservableList.add(new Team(99, "<add new>"));

        participantsCompetitionsTypes.add(new CompetitionSubTapes());

        dlinnoeOrujieList.addAll("Gun shu", "Qiang shu", "Nan gun");
        korotkoeOrujieList.addAll("Dao shu", "Jian shu", "Nan dao", "Tai ji jian");
        kulakList.addAll("Chang quan", "Nan quan", "Tai ji quan");

    }

}
