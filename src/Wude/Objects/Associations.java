package Wude.Objects;

public class Associations {

    private int associationID;
    private String associationName;

public Associations(int i, String st){
    setAssociationID(i);
    setAssociationName(st);

}
    public int getAssociationID() {
        return associationID;
    }

    public void setAssociationID(int associationID) {
        this.associationID = associationID;
    }

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    @Override
    public String toString() {
        return this.associationName;
    }


}
